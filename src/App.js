import React from "react";
import InlineCss from "./learnComponent/InlineCss";
import TernaryOperator from "./learnComponent/TernaryOperator";
import LearnUseState1 from "./LearnUseState/LearnUseState1";
import LearnUseState2 from "./LearnUseState/LearnUseState2";
import LearnUseState3 from "./LearnUseState/LearnUseState3";
import LearnUseState4 from "./LearnUseState/LearnUseState4";
import WhyUseState5 from "./LearnUseState/WhyUseState5";
import LearnUseState5 from "./LearnUseState/LearnUseState5";
import LearnUseState6 from "./LearnUseState/LearnUseState6";

const App = () => {
  return (
    <div>
      {/* <InlineCss></InlineCss> */}
      {/* <TernaryOperator></TernaryOperator> */}
      {/* <LearnUseState1></LearnUseState1> */}
      {/* <LearnUseState2></LearnUseState2> */}
      {/* <LearnUseState3></LearnUseState3> */}
      {/* <LearnUseState4></LearnUseState4> */}
      {/* <WhyUseState5></WhyUseState5> */}
      {/* <LearnUseState5></LearnUseState5> */}
      <LearnUseState6></LearnUseState6>
    </div>
  );
};

export default App;
