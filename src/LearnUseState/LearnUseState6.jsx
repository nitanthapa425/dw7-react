import React, { useState } from "react";

const LearnUseState6 = () => {
  let [str, setStr] = useState("nitan");

  // sint str is string thus before rendering it check weather value is same as
  //since arr is non primitive thus before rendering it check weather memory location is same
  let [arr, setArr] = useState([1, 2, 3]);

  console.log("i am run");

  return (
    <div>
      <button
        onClick={() => {
          setArr([1, 2, 3]);
        }}
      >
        {" "}
        change Arr
      </button>
      <br></br>

      <button
        onClick={() => {
          setStr("nitan");
        }}
      >
        {" "}
        change Str
      </button>
    </div>
  );
};

export default LearnUseState6;
