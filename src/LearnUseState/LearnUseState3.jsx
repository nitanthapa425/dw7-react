import React, { useState } from "react";

const LearnUseState3 = () => {
  let [showError, setShowError] = useState(true);
  return (
    <div>
      {showError ? <div style={{ color: "red" }}>this is error</div> : null}
      <br></br>

      <button
        onClick={() => {
          setShowError(true);
        }}
      >
        Show Error
      </button>
      <br></br>
      <button
        onClick={() => {
          setShowError(false);
        }}
      >
        Hide Error
      </button>
    </div>
  );
};

export default LearnUseState3;
