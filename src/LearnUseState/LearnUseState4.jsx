import React, { useState } from "react";

const LearnUseState4 = () => {
  let [showError, setShowError] = useState(true); //false
  return (
    <div>
      {showError ? <div>Show Error</div> : null}

      <button
        onClick={() => {
          setShowError(!showError); //
        }}
      >
        {showError ? "Hide" : "Show"}
      </button>
    </div>
  );
};

export default LearnUseState4;
