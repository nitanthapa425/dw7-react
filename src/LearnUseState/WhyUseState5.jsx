import React, { useState } from "react";

const WhyUseState5 = () => {
  //   let [count, setCount] = useState(0); //0

  //a component will render if setCount or setName or set.... is called

  let count = 0;
  return (
    <div>
      <br></br>
      <button
        onClick={() => {
          count = count + 1;
        }}
      >
        Increment count
      </button>
      <br></br>
      count is {count}
      <br></br>
    </div>
  );
};

export default WhyUseState5;
