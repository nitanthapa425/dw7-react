import React, { useState } from "react";

const LearnUseState1 = () => {
  // let name="nitan"
  let [name, setName] = useState("nitan");
  let [age, setAge] = useState(0);
  return (
    <div>
      name is {name}
      <br></br>
      age is {age}
      <br></br>
      <button
        onClick={() => {
          //   name = "ram";
          setName("ram");
        }}
      >
        Change name
      </button>
      <br></br>
      <button
        onClick={() => {
          setAge(29);
        }}
      >
        Change age
      </button>
      <br></br>
      <button
        onClick={() => {
          setName("hari");
          setAge(50);
        }}
      >
        Change age and name
      </button>
    </div>
  );
};

export default LearnUseState1;
