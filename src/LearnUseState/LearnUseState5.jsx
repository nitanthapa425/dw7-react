import React, { useState } from "react";

const LearnUseState5 = () => {
  let [count, setCount] = useState(0); //0//1//2
  let [count1, setCount1] = useState(100); //100//101

  //   setCount(count + 1);
  return (
    <div>
      count is {count}
      <br></br>
      count1 is {count1}
      <br></br>
      <button
        onClick={() => {
          setCount(count + 1); //2
        }}
      >
        Increment count
      </button>
      <br></br>
      <button
        onClick={() => {
          setCount1(count1 + 1); //101
        }}
      >
        Increment count1
      </button>
    </div>
  );
};

export default LearnUseState5;
