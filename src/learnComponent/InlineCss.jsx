import React from "react";

const InlineCss = () => {
  //string = show
  //number = show
  //boolean  = is not show
  //null  = is not
  //undefined is not shown
  //array will show but it remove w
  //object we can not use object inside tag

  let bestFriendList = ["nitan", "ram", "hari"];

  let divBestFriendList = bestFriendList.map((value, i) => {
    return <div>my best friend is {value}</div>;
  });

  /**
   * [
   * <div>my best firend is nitan </div>,
   * <div>my best firend is hari </div>,
   * <div>my best firend is ram </div>
   *
   * ]
   */

  return (
    <div>
      <p style={{ backgroundColor: "red", color: "white", padding: "30px" }}>
        my name is nitan
      </p>
      <p className="bg-blue">i am 29</p>
      <p style={{ border: "solid red 3px" }}>hello workd</p>

      <button
        onClick={() => {
          console.log("button is clicked");
        }}
        style={{ cursor: "pointer" }}
      >
        Click me
      </button>

      <p
        onClick={() => {
          console.log("we can use onClick props in any tag");
        }}
        style={{ cursor: "pointer" }}
      >
        nitan
      </p>

      {divBestFriendList}
    </div>
  );
};

export default InlineCss;
