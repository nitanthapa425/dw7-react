import React from "react";

const TernaryOperator = () => {
  // age>=18<div>he can enter room</div>
  // else<div>he can not enter room</div>

  // (age === 16)  <div>your age is 16</div>
  //          (age === 17)  <div>your age is 17</div>
  //          (age === 18)  <div>your ageis 18</div>
  //        else <div>your age is neither 16 ,17,18</div>

  let age = 25;
  return (
    <div>
      {age >= 18 ? (
        <div>he can enter room</div>
      ) : (
        <div>he can not enter room</div>
      )}

      {age === 16 ? (
        <div>your age is 16</div>
      ) : age === 17 ? (
        <div>your age is 17</div>
      ) : age === 18 ? (
        <div>your age is 18</div>
      ) : (
        <div>your age is neither 16,17,18</div>
      )}
    </div>
  );
};

export default TernaryOperator;
